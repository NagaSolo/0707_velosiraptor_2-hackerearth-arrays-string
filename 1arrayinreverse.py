"""
Given the size and the elements of array A, print all the elements in reverse order.

Input:
First line of input contains, N - size of the array.
Following N lines, each contains one integer, i{th} element of the array i.e. A[i].

Output:
Print all the elements of the array in reverse order, each element in a new line.

Constraints:
N = range(1, 100)
A[i] = range(1, 1000)

"""
N = int(input())
A = []
for i in range(N):
    i = int(input())
    A.append(i)
A.reverse() # reverse A inplace

for i in range(len(A)):
    print(A[i])