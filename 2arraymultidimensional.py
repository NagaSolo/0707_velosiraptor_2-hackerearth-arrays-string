""" Problem Statement

    Transpose
    Given a 2D array A, your task is to convert all rows to columns and columns to rows.

    Input:
    First line contains 2 space separated integers, N - total rows, M - total columns.
    Each of the next N lines will contain M space separated integers.

    Output:
    Print M lines each containing N space separated integers.

    Constraints:
    N = range(1,10)
    M = range(1,10)
    A[i][j] = range(0,100) where i = range(i, N) and j = range(j, M)

    SAMPLE INPUT 
    3 5
    13 4 8 14 1
    9 6 3 7 21
    5 12 17 9 3

    SAMPLE OUTPUT 
    13 9 5
    4 6 12
    8 3 17
    14 7 9
    1 21 3

"""
matrix_dim_input = input().split()
N,M = matrix_dim_input
print(range(int(N))
# [[map(int, input().split()) for M in range(int(M))] for N in range(int(N))]

for n in range(int(N)):
    for m in range(int(M)):
        m = input().split()
        print(m)