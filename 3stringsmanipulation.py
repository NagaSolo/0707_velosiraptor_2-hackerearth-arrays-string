''' sort the substring, from hackerearth CodeMonk

    for input, first time implementing sys module
'''

import sys

# function for getting input using sys, not input
def input_getter():
    return map(int, sys.stdin.readline().strip().split())

T = input_getter()
